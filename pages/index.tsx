/* eslint-disable react/no-deprecated */
import { Inter } from '@next/font/google'
import { Checkbox, Table } from 'flowbite-react';
import React from 'react'

const inter = Inter({ subsets: ['latin'] })

const TableHeader = [
  {title: "주문일시"},
  {title: "주문번호"},
  {title: "상품코드"},
  {title: "상품명"},
  {title: "판매가"},
  {title: "수량"},
  {title: "배송", details: ["택배사"]},
  {title: "운송장번호"},
  {title: "받는사람", details: ["이름", "휴대폰", "주소", "배송요청사항"]},
  {title: "주문 취소하기"},
  {title: "배송지연"},
]

export default function Home() {
  return (
      <Table hoverable={true}>
  <Table.Head>
    <Table.HeadCell className="!p-4">
      <Checkbox />
    </Table.HeadCell>
    <Table.HeadCell>
      Product name
    </Table.HeadCell>
    <Table.HeadCell>
      <span className="sr-only">
        Edit
      </span>
    </Table.HeadCell>
    <Table.HeadCell>
      Category
    </Table.HeadCell>
    <Table.HeadCell>
      Price
    </Table.HeadCell>
    <Table.HeadCell>
      <span className="sr-only">
        Edit
      </span>
    </Table.HeadCell>
  </Table.Head>
  <Table.Body className="divide-y">
    <Table.Row className="bg-white dark:border-gray-700 dark:bg-gray-800">
      <Table.Cell className="!p-4">
        <Checkbox />
      </Table.Cell>
      <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
        Apple MacBook Pro 17"
      </Table.Cell>
      <Table.Cell>
        Laptop
      </Table.Cell>
      <Table.Cell>
        $2999
      </Table.Cell>
      <Table.Cell>
        <a
          href="/tables"
          className="font-medium text-blue-600 hover:underline dark:text-blue-500"
        >
          Edit
        </a>
      </Table.Cell>
    </Table.Row>
    <Table.Row className="bg-white dark:border-gray-700 dark:bg-gray-800">
      <Table.Cell className="!p-4">
        <Checkbox />
      </Table.Cell>
      <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
        Microsoft Surface Pro
      </Table.Cell>
      <Table.Cell>
        White
      </Table.Cell>
      <Table.Cell>
        Laptop PC
      </Table.Cell>
      <Table.Cell>
        $1999
      </Table.Cell>
      <Table.Cell>
        <a
          href="/tables"
          className="font-medium text-blue-600 hover:underline dark:text-blue-500"
        >
          Edit
        </a>
      </Table.Cell>
    </Table.Row>
    <Table.Row className="bg-white dark:border-gray-700 dark:bg-gray-800">
      <Table.Cell className="!p-4">
        <Checkbox />
      </Table.Cell>
      <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
        Magic Mouse 2
      </Table.Cell>
      <Table.Cell>
        Black
      </Table.Cell>
      <Table.Cell>
        Accessories
      </Table.Cell>
      <Table.Cell>
        $99
      </Table.Cell>
      <Table.Cell>
        <a
          href="/tables"
          className="font-medium text-blue-600 hover:underline dark:text-blue-500"
        >
          Edit
        </a>
      </Table.Cell>
    </Table.Row>
  </Table.Body>
</Table>
    );
}
